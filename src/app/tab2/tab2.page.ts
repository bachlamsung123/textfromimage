import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { createWorker } from 'tesseract.js';
import { UserPhoto, PhotoService } from '../services/photo.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  worker: Tesseract.Worker;
  workerReady = false;
  ocrResult = '';
  captureProgress = 0;

  constructor(public photoService: PhotoService, public actionSheetController: ActionSheetController) {
    this.loadWorker();
  }

  async ngOnInit() {
  }

  public async showActionSheet(photo: UserPhoto) {

    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
        }
      }]
    });
    await actionSheet.present();
  }

  async loadWorker() {
    this.worker = await createWorker({
      logger: progress => {
        console.log(progress)
        if (progress.status == 'recognizing text') {
          this.captureProgress = parseInt('' + progress.progress * 100);
        }
      }
    });
    await this.worker.load();
    await this.worker.loadLanguage('eng');
    await this.worker.initialize('eng');
    console.log('FIN');

    this.workerReady = true;
  }

  async recognizeImage() {
    console.log('clicked');
    const result = await this.worker.recognize(this.photoService.photo.webviewPath);
    console.log(result);
    this.ocrResult = result.data.text;
  }
}
